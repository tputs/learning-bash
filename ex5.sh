#!/bin/bash
# extract usernames from ldap strings
for i in $(cat ldapusers)
do
		USER=${i%%,*}
		USER=${USER#*=}
		echo $USER >> users
done

# show that user creation will work
for j in $(cat users)
do
	echo useradd $j
done

exit 0
