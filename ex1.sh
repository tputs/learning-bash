#!/bin/bash
#
# copy contents of /var/log/messages to /var/log/messages.log and delete the contents of the /var/log/messages file

cat var/log/messages >> var/log/messages.log && rm var/log/messages

# echo sample text into a new file
#touch var/log/messages && echo "sample text" >> var/log/messages
