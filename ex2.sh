#!/bin/bash
#
# copy all files of which the names are provided as arguments on the command line to the users home directory.
# if no files have been provided, use read to ask the user

if [ -z $1 ]
then
	echo provide filenames
	read FILENAMES
else
	FILENAMES="$@"
fi

echo the following filenames have been provided: $FILENAMES

for i in $FILENAMES
do
	cp $i $HOME
done
