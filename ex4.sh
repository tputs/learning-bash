#!/bin/bash

USER=cn=lara,dc=example,dc=com
# remove everything after the first comma
USER=${USER%%,*}
# remove everything before the last equal
USER=${USER#*=}

USER=$(echo $USER | tr [:lower:] [:upper:])

echo the username is $USER
